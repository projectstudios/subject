# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased
Nothing major to note.

## 0.1.0-alpha 2022-8-29
**0.1.0-alpha** aims to bring a base that other developers can work on and help build **Subject.**
### Features
- Python Package ([Pypi](https://pypi.org/project/Subjectpy/))
- Add basic add function