# Subject
Subject is a multi function application that aims to bring many types of functions and tools developers can use to build and run their applications.

# Releases
You can download the python package by doing the following command. 

```pip install Subjectpy```

You will download the latest version thats ready to be downloaded. (All files come from [PyPi](https://pypi.org/project/Subjectpy/))